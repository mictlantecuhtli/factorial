/*
Autor: Fatima Azucena MC
Fecha: 08_12_2020
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcular el factorial de un numero*/

//Libreria principal
#include <stdio.h>
int main(){//Inicio metodo principal
	//Declaracion de variables
	int num, numero_factorial = 1;

	//Entrada de datos
	printf("Ingrese un numero: ");
	scanf("%d", &num);

	//Mediante este ciclo for podemos calcular el
	//factorial que el usuario ingreso
	for (int i = 1; i<= num; i++){
		numero_factorial = numero_factorial * i;
	}
	printf("El factorial de %d %s %d", num ," es: ",factorial );
}//Fin metodo principal
